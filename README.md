# MailHandler

## Usage

Configuration
````
const mailHandler = require('mail-handler-nt');

// Same as transport configuration for Nodemailer
// See https://github.com/nodemailer/nodemailer/tree/master/examples

const transportConfiguration = 
{
  "host": "smtp-mail.outlook.com",
  "port": 587,
  "secure": false,
  "auth":
  {
    "user": "test@test.com",
    "pass": "test"
  },
  "tls":
  {
    "ciphers":"SSLv3"
  }
};

mailHandler.configure(transportConfiguration);
````

In any other file

````
const mailHandler = require('mail-handler-nt').transporter();

// sendMail is a function from nodemailer
// See https://github.com/nodemailer/nodemailer/blob/master/examples/full.js
mailHandler.sendMail(
{
   from: "test@test.com",
   to: "user@email.com",
   subject: "Email verification - NikosiTech",
   html:
    "<h1>Hello Jack Durand</h1>"
})
.then(data =>
{
	// EMAIL WAS SENT
  return true;
})
.catch(err =>
{
	// AN ERROR OCCURS
  return err;
});
