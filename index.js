const nodemailer = require('nodemailer');

let SMTP_CONFIG = null;

/**
 * Set config object
 * @param {Object} smtp
 */
function configure(smtp)
{
  SMTP_CONFIG = smtp;
}


/**
 * Get transporter
 * @param {Object} smtp
 */
function createTransport(smtp = null)
{
  if (!smtp)
    smtp = SMTP_CONFIG;

  return nodemailer.createTransport(smtp);
}


module.exports =
{
  configure,
  transporter: createTransport
};